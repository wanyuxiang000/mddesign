package com.mgface.metadata.design.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 9:36
 **/
@Data
public class ObjectVo implements Serializable {
    @ApiModelProperty("对象ID")
    private String objID;

    @ApiModelProperty(value = "组织ID(租户ID)",required = true)
    private String orgID;

    @ApiModelProperty(value = "应用ID")
    private String appID;

    @ApiModelProperty(value = "对象名称")
    private String objName;

    @ApiModelProperty(value = "对象的显示名称")
    private String label;

    @ApiModelProperty("对象描述")
    private String descrption;

    @ApiModelProperty(value = "是否启用该对象(Y:启用，N:不启用)", allowableValues = "Y,N")
    private String isEnabled;

    @ApiModelProperty(value = "操作员ID")
    private String operationID;

}
