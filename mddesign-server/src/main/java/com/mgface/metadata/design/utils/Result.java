package com.mgface.metadata.design.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 18:20
 **/
@Data
public final class Result<T> {

    private Result() {

    }

    private int code;
    private String message;

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(String message) {
        this.code = 200;
        this.message = message;
    }

    public Result(T data) {
        this.code = 200;
        this.message = JSONObject.toJSONString(data);

    }

    public static Result ok() {
        return new Result("success");
    }
}
