package com.mgface.metadata.design.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 14:01
 **/
@Data
public class FieldQueryVo {
    @ApiModelProperty(value = "组织ID", required = true)
    private String orgID;
    @ApiModelProperty(value = "APPID")
    private String appID;
    @ApiModelProperty(value = "对象名称")
    private String objName;
    @ApiModelProperty(value = "字段名")
    private String fieldName;
    @ApiModelProperty(value = "字段ID")
    private String fieldID;
}
