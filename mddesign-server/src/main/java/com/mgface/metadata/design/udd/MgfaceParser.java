package com.mgface.metadata.design.udd;

import org.apache.calcite.sql.SqlNode;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-06-07 15:48
 **/
public interface MgfaceParser {
    /**
     * 从SQL提出去元数据信息
     */
    Map<String, List<?>> extractMeta(SqlNode sqlNode, String sql) throws Exception;

    /**
     * 权限校验功能
     * //todo 这个建议把元数据表的数据同步到redis，直接和redis进行交互，而不需要频繁的查询database
     */
    Map<String, Map<String, String>> authVerified(DataSource ds, Map<String, List<?>> data) throws Exception;

    /**
     * 生成物理底层SQL
     */
    String physicalQuery(String sql, Map<String, Map<String, String>> expData) throws Exception;

    //执行操作
    default String exec(DataSource ds, SqlNode sqlNode, String sql) throws Exception {
        Map<String, List<?>> data = extractMeta(sqlNode, sql);
        Map<String, Map<String, String>> result = authVerified(ds, data);
        return physicalQuery(sql, result);
    }
}
