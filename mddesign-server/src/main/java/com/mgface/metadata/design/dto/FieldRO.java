package com.mgface.metadata.design.dto;

import lombok.Data;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-27 16:18
 **/
@Data
public class FieldRO {
    private Integer fieldnum;
    private String objID;
}
