package com.mgface.metadata.design.dao;

import com.mgface.metadata.design.dto.DataDTO;
import com.mgface.metadata.design.dto.FieldDTO;
import com.mgface.metadata.design.utils.BaseTkMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.SQLException;
import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:21
 **/
@Mapper
public interface DataOperationMapper extends BaseTkMapper<DataDTO> {
    //当orgID为-1的时候，说明该对象是继承系统已经存在的字段属性
    @Select("select * from fields where  objID = #{objID} and orgID = #{orgID}")
    List<FieldDTO> selectFieldsByOrgIDObjID(@Param("orgID") String orgID, @Param("objID") String objID) throws SQLException;
}
