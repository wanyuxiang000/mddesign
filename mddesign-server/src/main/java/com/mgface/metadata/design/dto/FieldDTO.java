package com.mgface.metadata.design.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:36
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "字段DTO", parent = BaseDTO.class)
@Table(name = "fields")
public class FieldDTO extends BaseDTO {
    @ApiModelProperty("字段的唯一标识")
    @Column(name = "fieldID")
    @Id
    private String fieldID;
    @ApiModelProperty("字段所属对象的 ObjID")
    @Column(name = "objID")
    private String objID;
    @ApiModelProperty("字段名")
    @Column(name = "fieldName")
    private String fieldName;
    @ApiModelProperty("字段中文描述")
    @Column(name = "descrption")
    private String descrption;
    @ApiModelProperty("字段展示名称")
    @Column(name = "label")
    private String label;
    @ApiModelProperty(value = "对应到 Data 数据表的数据存储字段映射", example = "0")
    @Column(name = "fieldNum")
    private Integer fieldNum;
    @ApiModelProperty("字段的数据类型包含:Number、TEXT、Auto Number、Date/Time、Email、Text Area等,也包含特殊的关系类型如:Look-up关系类型,Master-Detail关系类型等")
    @Column(name = "dataType")
    private String dataType;
    @ApiModelProperty("用于Number、Currency、Geolocation等数字数据类型的关联设定")
    @Column(name = "digitLeft")
    private String digitLeft;
    @ApiModelProperty("小数部分的最大位数Scale")
    @Column(name = "scale")
    private String scale;
    @ApiModelProperty("数据类型为TEXT时启用，指定TEXT类型的字符的长度限制")
    @Column(name = "textLength")
    private String textLength;
    @ApiModelProperty("DateType为关系类型(Look-up,Master-Detail等)时会启用，其中RelatedTo保存关联的应用对象ID")
    @Column(name = "relatedTo")
    private String relatedTo;
    @ApiModelProperty("DateType为关系类型(Look-up,Master-Detail等)时会启用,ChildRelationshipName用于保存父子关系中子方的关系名称，同一个父对象的子方的关系名称唯一，用于关系的反向查询")
    @Column(name = "childRelationshipName")
    private String childRelationshipName;
    @ApiModelProperty("是否必须(Y:是,N:否)")
    @Column(name = "isRequired")
    private String isRequired;
    @ApiModelProperty("是否允许重复值(Y:是,N:否)")
    @Column(name = "isUnique")
    private String isUnique;
    @ApiModelProperty("是否启用(Y:是,N:否)")
    @Column(name = "isEnabled")
    private String isEnabled;
    @ApiModelProperty("是否需要建索引(Y:是,N:否)")
    @Column(name = "isIndexed")
    private String isIndexed;
    @ApiModelProperty("是否作为查询条件(Y:是,N:否)")
    @Column(name = "isSearched")
    private String isSearched;
}
