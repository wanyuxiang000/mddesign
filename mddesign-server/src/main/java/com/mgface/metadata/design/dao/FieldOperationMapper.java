package com.mgface.metadata.design.dao;

import com.mgface.metadata.design.dto.FieldDTO;
import com.mgface.metadata.design.dto.FieldRO;
import com.mgface.metadata.design.utils.BaseTkMapper;
import com.mgface.metadata.design.vo.FieldQueryVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.SQLException;
import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:21
 **/
@Mapper
public interface FieldOperationMapper extends BaseTkMapper<FieldDTO> {
    @Select("select max(ifnull(a.fieldNum,-1))+1 as fieldnum,max(b.objID) as objID from fields a right join objects b on a.objID = b.objID where b.orgID =#{orgID} and b.appID =#{appID} and b.objName=#{objName}")
    FieldRO selectMaxByOrgIDObjID(@Param("orgID") String orgID, @Param("appID") String appID, @Param("objName") String objName) throws SQLException;

    @Select("select a.* from fields a left join objects b on a.objID = b.objID where b.orgID =#{orgID} and b.appID =#{appID} and b.objName=#{objName}")
    List<FieldDTO> selectAllField(FieldQueryVo fieldQueryVo) throws SQLException;

}
