package com.mgface.metadata.design.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:35
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "对象DTO", parent = BaseDTO.class)
@Table(name = "objects")
public class ObjectDTO extends BaseDTO {

    @ApiModelProperty("应用ID")
    @Column(name = "appID")
    private String appID;//应用层面的ID

    @ApiModelProperty("对象ID")
    @Column(name = "objID")
    @Id
    private String objID;

    @ApiModelProperty("对象名称")
    @Column(name = "objName")
    private String objName;

    @ApiModelProperty("对象的显示名称")
    @Column(name = "label")
    private String label;

    @ApiModelProperty("对象描述")
    @Column(name = "descrption")
    private String descrption;

    @ApiModelProperty("创建者")
    @Column(name = "createdBy")
    private String createdBy;

    @ApiModelProperty("创建时间")
    @Column(name = "createdDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    @ApiModelProperty("最后修改者")
    @Column(name = "lastModifedBy")
    private String lastModifedBy;

    @ApiModelProperty("最后修改时间")
    @Column(name = "lastModifedDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifedDate;

    @ApiModelProperty("是否启用该对象（Y：启用，N：不启用）")
    @Column(name = "isEnabled")
    private String isEnabled;

    @ApiModelProperty("对象拥有者ID（是谁创建的，对象归属权）")
    @Column(name = "ownedId")
    private String ownedId;
}
