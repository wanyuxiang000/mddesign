package com.mgface.metadata.design.datatypecheck.impl;

import com.mgface.metadata.design.datatypecheck.DataType;
import com.mgface.metadata.design.datatypecheck.DataTypeCheck;
import org.springframework.stereotype.Component;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-27 13:48
 **/
@Component
@DataType("Geo")
public class GeolocationCheck implements DataTypeCheck<String> {
    @Override
    public boolean validation(String s) {
        return true;
    }
}
