package com.mgface.metadata.design.datatypecheck.impl;

import com.mgface.metadata.design.datatypecheck.DataType;
import com.mgface.metadata.design.datatypecheck.DataTypeCheck;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-27 13:48
 **/
@Component
@DataType("URL")
public class URLCheck implements DataTypeCheck<String> {
    private static final String regex = "^([hH][tT]{2}[pP]:/*|[hH][tT]{2}[pP][sS]:/*|[fF][tT][pP]:/*)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+(\\?{0,1}(([A-Za-z0-9-~]+\\={0,1})([A-Za-z0-9-~]*)\\&{0,1})*)$";
    private static final Pattern pattern = Pattern.compile(regex);
    @Override
    public boolean validation(String url) {
        return pattern.matcher(url).matches();
    }
}