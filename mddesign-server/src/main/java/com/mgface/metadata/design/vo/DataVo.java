package com.mgface.metadata.design.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 15:26
 **/
@Data
public class DataVo {
    @ApiModelProperty(value = "全局唯一标识")
    private String GUID;
    @ApiModelProperty(value = "组织ID(租户ID)", required = true)
    private String orgID;
    @ApiModelProperty(value = "应用ID", required = true, position = 1)
    private String appID;
    @ApiModelProperty(value = "数据所属的objID", required = true, position = 2)
    private String objID;
    @ApiModelProperty(value = "数据名称", required = true, position = 3)
    private String dataName;
    @ApiModelProperty(value = "数据的详细内容JSON", required = true, position = 4, example = "{\"stationLocation\":\"广东省\",\"stationEmail\":\"111@stremax.com\",\"stationCurrency\":\"$美元\"}")
    private String jsonData;
}
