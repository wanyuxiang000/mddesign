package com.mgface.metadata.design.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:36
 **/
@EqualsAndHashCode(callSuper = true)
@ApiModel("数据DTO")
@Data
@Table(name = "datas")
public class DataDTO extends BaseDTO {
    @ApiModelProperty("全局唯一标识")
    @Column(name = "GUID")
    @Id
    private String GUID;
    @ApiModelProperty("对象ID")
    @Column(name = "objID")
    private String objID;
    @ApiModelProperty("应用ID")
    @Column(name = "appID")
    private String appID;//应用层面的ID
    @ApiModelProperty("数据别名")
    @Column(name = "name")
    private String name;
    @ApiModelProperty("fileds表字段fieldsnum0对应的实际值")
    @Column(name = "value0")
    private String value0;
    @ApiModelProperty("fileds表字段fieldsnum1对应的实际值")
    @Column(name = "value1")
    private String value1;
    @ApiModelProperty("fileds表字段fieldsnum2对应的实际值")
    @Column(name = "value2")
    private String value2;
    @ApiModelProperty("fileds表字段fieldsnum3对应的实际值")
    @Column(name = "value3")
    private String value3;
    @ApiModelProperty("fileds表字段fieldsnum4对应的实际值")
    @Column(name = "value4")
    private String value4;
    @ApiModelProperty("fileds表字段fieldsnum5对应的实际值")
    @Column(name = "value5")
    private String value5;
    @ApiModelProperty("fileds表字段fieldsnum6对应的实际值")
    @Column(name = "value6")
    private String value6;
    @ApiModelProperty("fileds表字段fieldsnum7对应的实际值")
    @Column(name = "value7")
    private String value7;
    @ApiModelProperty("fileds表字段fieldsnum8对应的实际值")
    @Column(name = "value8")
    private String value8;
    @ApiModelProperty("fileds表字段fieldsnum9对应的实际值")
    @Column(name = "value9")
    private String value9;
    @ApiModelProperty("fileds表字段fieldsnum10对应的实际值")
    @Column(name = "value10")
    private String value10;
    @ApiModelProperty("fileds表字段fieldsnum11对应的实际值")
    @Column(name = "value11")
    private String value11;
    @ApiModelProperty("fileds表字段fieldsnum12对应的实际值")
    @Column(name = "value12")
    private String value12;
    @ApiModelProperty("fileds表字段fieldsnum13对应的实际值")
    @Column(name = "value13")
    private String value13;
    @ApiModelProperty("fileds表字段fieldsnum14对应的实际值")
    @Column(name = "value14")
    private String value14;
    @ApiModelProperty("fileds表字段fieldsnum15对应的实际值")
    @Column(name = "value15")
    private String value15;
    @ApiModelProperty("fileds表字段fieldsnum16对应的实际值")
    @Column(name = "value16")
    private String value16;
    @ApiModelProperty("fileds表字段fieldsnum17对应的实际值")
    @Column(name = "value17")
    private String value17;
    @ApiModelProperty("fileds表字段fieldsnum18对应的实际值")
    @Column(name = "value18")
    private String value18;
    @ApiModelProperty("fileds表字段fieldsnum19对应的实际值")
    @Column(name = "value19")
    private String value19;
    @ApiModelProperty("fileds表字段fieldsnum20对应的实际值")
    @Column(name = "value20")
    private String value20;
    @ApiModelProperty("fileds表字段fieldsnum21对应的实际值")
    @Column(name = "value21")
    private String value21;
    @ApiModelProperty("fileds表字段fieldsnum22对应的实际值")
    @Column(name = "value22")
    private String value22;
    @ApiModelProperty("fileds表字段fieldsnum23对应的实际值")
    @Column(name = "value23")
    private String value23;
    @ApiModelProperty("fileds表字段fieldsnum24对应的实际值")
    @Column(name = "value24")
    private String value24;
    @ApiModelProperty("fileds表字段fieldsnum25对应的实际值")
    @Column(name = "value25")
    private String value25;
    @ApiModelProperty("fileds表字段fieldsnum26对应的实际值")
    @Column(name = "value26")
    private String value26;
    @ApiModelProperty("fileds表字段fieldsnum27对应的实际值")
    @Column(name = "value27")
    private String value27;
    @ApiModelProperty("fileds表字段fieldsnum28对应的实际值")
    @Column(name = "value28")
    private String value28;
    @ApiModelProperty("fileds表字段fieldsnum29对应的实际值")
    @Column(name = "value29")
    private String value29;
    @ApiModelProperty("fileds表字段fieldsnum30对应的实际值")
    @Column(name = "value30")
    private String value30;
    @ApiModelProperty("fileds表字段fieldsnum31对应的实际值")
    @Column(name = "value31")
    private String value31;
    @ApiModelProperty("fileds表字段fieldsnum32对应的实际值")
    @Column(name = "value32")
    private String value32;
    @ApiModelProperty("fileds表字段fieldsnum33对应的实际值")
    @Column(name = "value33")
    private String value33;
    @ApiModelProperty("fileds表字段fieldsnum34对应的实际值")
    @Column(name = "value34")
    private String value34;
    @ApiModelProperty("fileds表字段fieldsnum35对应的实际值")
    @Column(name = "value35")
    private String value35;
    @ApiModelProperty("fileds表字段fieldsnum36对应的实际值")
    @Column(name = "value36")
    private String value36;
    @ApiModelProperty("fileds表字段fieldsnum37对应的实际值")
    @Column(name = "value37")
    private String value37;
    @ApiModelProperty("fileds表字段fieldsnum38对应的实际值")
    @Column(name = "value38")
    private String value38;
    @ApiModelProperty("fileds表字段fieldsnum39对应的实际值")
    @Column(name = "value39")
    private String value39;
    @ApiModelProperty("fileds表字段fieldsnum40对应的实际值")
    @Column(name = "value40")
    private String value40;
    @ApiModelProperty("fileds表字段fieldsnum41对应的实际值")
    @Column(name = "value41")
    private String value41;
    @ApiModelProperty("fileds表字段fieldsnum42对应的实际值")
    @Column(name = "value42")
    private String value42;
    @ApiModelProperty("fileds表字段fieldsnum43对应的实际值")
    @Column(name = "value43")
    private String value43;
    @ApiModelProperty("fileds表字段fieldsnum44对应的实际值")
    @Column(name = "value44")
    private String value44;
    @ApiModelProperty("fileds表字段fieldsnum45对应的实际值")
    @Column(name = "value45")
    private String value45;
}
