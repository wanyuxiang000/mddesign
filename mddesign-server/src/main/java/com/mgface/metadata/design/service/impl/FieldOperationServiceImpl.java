package com.mgface.metadata.design.service.impl;

import com.mgface.metadata.design.dao.FieldOperationMapper;
import com.mgface.metadata.design.dto.FieldDTO;
import com.mgface.metadata.design.dto.FieldRO;
import com.mgface.metadata.design.service.FieldOperationService;
import com.mgface.metadata.design.utils.SnowFlakeUtil;
import com.mgface.metadata.design.vo.FieldQueryVo;
import com.mgface.metadata.design.vo.FieldVo;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 11:18
 **/
@SuppressWarnings("Duplicates")
@Service
public class FieldOperationServiceImpl implements FieldOperationService<FieldDTO, FieldVo> {
    private final FieldOperationMapper fieldOperationMapper;

    public FieldOperationServiceImpl(FieldOperationMapper fieldOperationMapper) {
        this.fieldOperationMapper = fieldOperationMapper;
    }


    @SneakyThrows
    @Override
    public String add(FieldVo fieldVo) {
        //todo 要判断添加字段唯一性，不能重名，对于一个组织+对象区分出来
        FieldDTO fieldDTO = new FieldDTO();
        fieldDTO.setFieldID(SnowFlakeUtil.getId());
        fieldDTO.setOrgID(fieldVo.getOrgID());
        fieldDTO.setFieldName(fieldVo.getFieldName());
        fieldDTO.setDescrption(fieldVo.getDescrption());
        fieldDTO.setLabel(fieldVo.getLabel());
        FieldRO fieldRO = fieldOperationMapper.selectMaxByOrgIDObjID(fieldVo.getOrgID(), fieldVo.getAppID(), fieldVo.getObjName());
        fieldDTO.setObjID(fieldRO.getObjID());
        fieldDTO.setFieldNum(fieldRO.getFieldnum());
        fieldDTO.setDataType(fieldVo.getDataType());
        fieldDTO.setDigitLeft(fieldVo.getDigitLeft());
        fieldDTO.setScale(fieldVo.getScale());
        fieldDTO.setTextLength(fieldVo.getTextLength());
        fieldDTO.setRelatedTo(fieldVo.getRelatedTo());
        fieldDTO.setChildRelationshipName(fieldVo.getChildRelationshipName());
        fieldDTO.setIsRequired(fieldVo.getIsRequired());
        fieldDTO.setIsUnique(fieldVo.getIsUnique());
        fieldDTO.setIsEnabled(fieldVo.getIsEnabled());
        fieldDTO.setIsIndexed(fieldVo.getIsIndexed());
        fieldDTO.setIsSearched(fieldVo.getIsSearched());
        return fieldOperationMapper.insert(fieldDTO) == 1 ? fieldDTO.getFieldID() : "insert error";
    }

    @Override
    public FieldDTO get(FieldQueryVo fieldQueryVo) {
        Example example = new Example(FieldDTO.class);
        example.createCriteria()
                .andEqualTo("orgID", fieldQueryVo.getOrgID())
                .andEqualTo("fieldID", fieldQueryVo.getFieldID());
        return fieldOperationMapper.selectOneByExample(example);
    }

    @Override
    public int delete(FieldQueryVo fieldQueryVo) {
        Example example = new Example(FieldDTO.class);
        example.createCriteria().andEqualTo("fieldName", fieldQueryVo.getFieldName())
                .andEqualTo("orgID", fieldQueryVo.getOrgID())
                .andEqualTo("fieldID", fieldQueryVo.getFieldID());
        return fieldOperationMapper.deleteByExample(example);
    }

    @Override
    public int update(FieldVo fieldVo) {
        FieldDTO objectDTO = new FieldDTO();
        objectDTO.setFieldName(fieldVo.getFieldName());
        objectDTO.setDescrption(fieldVo.getDescrption());
        objectDTO.setLabel(fieldVo.getLabel());
        objectDTO.setDataType(fieldVo.getDataType());
        objectDTO.setDigitLeft(fieldVo.getDigitLeft());
        objectDTO.setScale(fieldVo.getScale());
        objectDTO.setTextLength(fieldVo.getTextLength());
        objectDTO.setRelatedTo(fieldVo.getRelatedTo());
        objectDTO.setChildRelationshipName(fieldVo.getChildRelationshipName());
        objectDTO.setIsRequired(fieldVo.getIsRequired());
        objectDTO.setIsUnique(fieldVo.getIsUnique());
        objectDTO.setIsSearched(fieldVo.getIsSearched());
        objectDTO.setIsEnabled(fieldVo.getIsEnabled());
        objectDTO.setIsIndexed(fieldVo.getIsIndexed());
        Example example = new Example(FieldDTO.class);
        example.createCriteria().andEqualTo("fieldID", fieldVo.getFieldID());
        return fieldOperationMapper.updateByExampleSelective(objectDTO, example);
    }

    @SneakyThrows
    @Override
    public List<FieldDTO> getAll(FieldQueryVo fieldQueryVo) {
        return fieldOperationMapper.selectAllField(fieldQueryVo);
    }
}
