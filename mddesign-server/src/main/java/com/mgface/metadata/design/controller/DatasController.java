package com.mgface.metadata.design.controller;

import com.mgface.metadata.design.dto.DataDTO;
import com.mgface.metadata.design.service.DataOperationService;
import com.mgface.metadata.design.utils.Result;
import com.mgface.metadata.design.vo.DataQueryVo;
import com.mgface.metadata.design.vo.DataVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:27
 **/
@Api(value = "数据义模型", description = "对定义数据的数据的CRUD操作", tags = {"数据CRUD操作"})
@RestController
@RequestMapping("datas")
public class DatasController {

    private final DataOperationService<DataDTO, DataVo> dataOperationService;

    public DatasController(DataOperationService<DataDTO, DataVo> dataOperationService) {
        this.dataOperationService = dataOperationService;
    }

    @PutMapping("/add")
    @ApiOperation(value = "添加一条数据操作")
    public Result addDatas(DataVo dataVo) {
        return new Result<>(dataOperationService.add(dataVo));
    }

    @GetMapping("/select")
    @ApiOperation(value = "查找一条数据操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GUID", value = "全局数据唯一标识", required = true)
    })
    public Result selectDatas(DataQueryVo dataQueryVo) {
        return new Result<>(dataOperationService.get(dataQueryVo));
    }

    @GetMapping("/selectAll")
    @ApiOperation(value = "查找指定对象的所有数据操作")
    public Result selectFields(DataQueryVo dataQueryVo) {
        return new Result<>(dataOperationService.getAll(dataQueryVo.getOrgID(), dataQueryVo.getObjectID(), dataQueryVo.getAppID()));
    }

    @DeleteMapping("/del")
    @ApiOperation(value = "删除一条数据操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GUID", value = "全局数据唯一标识", required = true)
    })
    public Result delDatas(DataQueryVo dataQueryVo) {
        dataOperationService.delete(dataQueryVo);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新一条数据操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GUID", value = "全局数据唯一标识", required = true)
    })
    public Result updateDatas(DataVo dataVo) {
        dataOperationService.update(dataVo);
        return Result.ok();
    }
}
