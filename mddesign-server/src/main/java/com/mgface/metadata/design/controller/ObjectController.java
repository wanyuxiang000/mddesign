package com.mgface.metadata.design.controller;

import com.mgface.metadata.design.dto.ObjectDTO;
import com.mgface.metadata.design.service.ObjectOperationService;
import com.mgface.metadata.design.utils.Result;
import com.mgface.metadata.design.vo.ObjectQueryVo;
import com.mgface.metadata.design.vo.ObjectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:27
 **/
@Api(value = "对象定义模型", description = "对定义对象的数据的CRUD操作", tags = {"对象CRUD操作"})
@RestController
@RequestMapping("object")
public class ObjectController {
    private final ObjectOperationService<ObjectDTO, ObjectVo> objectOperationService;

    public ObjectController(ObjectOperationService<ObjectDTO, ObjectVo> objectOperationService) {
        this.objectOperationService = objectOperationService;
    }

    @PutMapping("/add")
    @ApiOperation(value = "添加一个对象操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appID", value = "应用ID", required = true),
            @ApiImplicitParam(name = "objName", value = "对象名称", required = true),
            @ApiImplicitParam(name = "label", value = "对象的显示名称", required = true),
            @ApiImplicitParam(name = "isEnabled", value = "是否启用该对象(Y:启用，N:不启用)", required = true),
    })
    public Result addObject(ObjectVo objectVo) {
        return new Result<>(objectOperationService.add(objectVo));
    }

    @GetMapping("/select")
    @ApiOperation(value = "查找一个对象操作", notes = "objectID和objectName必须填写一个")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "objID", value = "对象ID"),
            @ApiImplicitParam(name = "objName", value = "对象名称")
    })
    public Result selectObject(ObjectQueryVo objectQueryVo) {
        if (StringUtils.isBlank(objectQueryVo.getObjID()) && StringUtils.isBlank(objectQueryVo.getObjName())) {
            return new Result("对象ID[objID]和对象名称[objName]必须有一个值");
        }
        return new Result<>(objectOperationService.get(objectQueryVo));
    }

    @GetMapping("/selectAll")
    @ApiOperation(value = "查找所有对象操作")
    public Result selectAllObjects(ObjectQueryVo dataQueryVo) {
        return new Result<>(objectOperationService.getAll(dataQueryVo));
    }

    @DeleteMapping("/del")
    @ApiOperation(value = "删除一个对象操作", notes = "objectID和objectName必须填写一个")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "objID", value = "对象ID"),
            @ApiImplicitParam(name = "objName", value = "对象名称")
    })
    public Result delObject(ObjectQueryVo objectQueryVo) {
        if (StringUtils.isBlank(objectQueryVo.getObjID()) && StringUtils.isBlank(objectQueryVo.getObjName())) {
            return new Result("对象ID[objID]和对象名称[objName]必须有一个值");
        }
        objectOperationService.delete(objectQueryVo);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新一个对象操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "objID", value = "对象ID", required = true)
    })
    public Result updateObject(ObjectVo objectVo) {
        objectOperationService.update(objectVo);
        return Result.ok();
    }
}
