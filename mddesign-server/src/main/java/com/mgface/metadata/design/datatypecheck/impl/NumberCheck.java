package com.mgface.metadata.design.datatypecheck.impl;

import com.mgface.metadata.design.datatypecheck.DataType;
import com.mgface.metadata.design.datatypecheck.DataTypeCheck;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-27 13:48
 **/
@Component
@DataType("Number")
public class NumberCheck implements DataTypeCheck<String> {
    private static final String regex = "\\d+|\\d*\\.\\d+|\\d*\\.?\\d+?e[+-]\\d*\\.?\\d+?|e[+-]\\d*\\.?\\d+?";
    private static final Pattern pattern = Pattern.compile(regex);

    @Override
    public boolean validation(String num) {
        return pattern.matcher(num).matches();
    }
}