package com.mgface.metadata.design.controller;

import com.mgface.metadata.design.udd.UDDEngine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-06-08 15:20
 **/
@Api("元模型的CRUD的操作接口")
@RestController
public class TestController {
    private final UDDEngine uddEngine;

    public TestController(UDDEngine uddEngine) {
        this.uddEngine = uddEngine;
    }

    @ApiOperation("test")
    @GetMapping("/test")
    public String test() {
        return uddEngine.exec("");
    }


}
