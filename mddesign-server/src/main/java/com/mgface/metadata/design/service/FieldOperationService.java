package com.mgface.metadata.design.service;

import com.mgface.metadata.design.vo.FieldQueryVo;

import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:16
 **/
public interface FieldOperationService<T, V> {
    String add(V fieldVo);

    T get(FieldQueryVo fieldQueryVo);

    int delete(FieldQueryVo fieldQueryVo);

    int update(V fieldVo);

    List<T> getAll(FieldQueryVo fieldQueryVo);
}
