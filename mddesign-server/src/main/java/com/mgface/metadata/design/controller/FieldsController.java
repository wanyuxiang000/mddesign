package com.mgface.metadata.design.controller;

import com.mgface.metadata.design.dto.FieldDTO;
import com.mgface.metadata.design.service.FieldOperationService;
import com.mgface.metadata.design.utils.Result;
import com.mgface.metadata.design.vo.FieldQueryVo;
import com.mgface.metadata.design.vo.FieldVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 17:27
 **/
@Api(value = "属性定义模型", description = "对定义属性的数据的CRUD操作", tags = {"字段CRUD操作"})
@RestController
@RequestMapping("fields")
public class FieldsController {
    private final FieldOperationService<FieldDTO, FieldVo> fieldOperationService;

    public FieldsController(FieldOperationService<FieldDTO, FieldVo> fieldOperationService) {
        this.fieldOperationService = fieldOperationService;
    }

    @PutMapping("/add")
    @ApiOperation(value = "添加一个属性操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldName", value = "字段名", required = true),
            @ApiImplicitParam(name = "label", value = "字段展示名称", required = true),
            @ApiImplicitParam(name = "objName", value = "字段所属对象名称", required = true),
            @ApiImplicitParam(name = "appID", value = "应用ID", required = true),
            @ApiImplicitParam(name = "isRequired", value = "是否必须(Y:是，N:否)", required = true),
            @ApiImplicitParam(name = "isUnique", value = "是否允许重复值(Y:是，N:否)", required = true),
            @ApiImplicitParam(name = "isEnabled", value = "是否启用(Y:是,N:否)", required = true),
            @ApiImplicitParam(name = "orgID", value = "组织ID(租户ID)", required = true)
    })
    public Result addFields(FieldVo fieldVo) {
        return new Result<>(fieldOperationService.add(fieldVo));
    }

    @GetMapping("/select")
    @ApiOperation(value = "查找一个属性操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldID", value = "字段名", required = true)
    })
    public Result selectFields(FieldQueryVo fieldQueryVo) {
        return new Result<>(fieldOperationService.get(fieldQueryVo));
    }

    @GetMapping("/selectAll")
    @ApiOperation(value = "查找指定对象的所有属性操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "objName", value = "字段所属对象名称", required = true),
            @ApiImplicitParam(name = "appID", value = "应用ID", required = true)
    })
    public Result selectAllFields(FieldQueryVo fieldQueryVo) {
        return new Result<>(fieldOperationService.getAll(fieldQueryVo));
    }

    @DeleteMapping("/del")
    @ApiOperation(value = "删除一个属性操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldID", value = "字段ID", required = true),
            @ApiImplicitParam(name = "fieldName", value = "字段名", required = true)
    })
    public Result delFields(FieldQueryVo fieldQueryVo) {
        fieldOperationService.delete(fieldQueryVo);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation(value = "更改一个属性操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldID", value = "字段的唯一标识", required = true),
            @ApiImplicitParam(name = "orgID", value = "组织ID(租户ID)", required = true)
    })
    public Result updateFields(FieldVo fieldDTO) {
        fieldOperationService.update(fieldDTO);
        return Result.ok();
    }
}
