package com.mgface.metadata.design;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@ComponentScan(value = {"com.mgface.metadata.design", "com.mgface.metadata"})
@MapperScan(basePackages = "com.mgface.metadata.design.dao")
public class MddesignApplication {
    public static void main(String[] args) {
        SpringApplication.run(MddesignApplication.class, args);
    }
}
