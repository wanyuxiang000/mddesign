package com.mgface.metadata.design.service.impl;

import com.mgface.metadata.design.dao.ObjectOperationMapper;
import com.mgface.metadata.design.dto.DataDTO;
import com.mgface.metadata.design.dto.ObjectDTO;
import com.mgface.metadata.design.service.ObjectOperationService;
import com.mgface.metadata.design.utils.SnowFlakeUtil;
import com.mgface.metadata.design.vo.ObjectQueryVo;
import com.mgface.metadata.design.vo.ObjectVo;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:23
 **/
@SuppressWarnings({"Duplicates", "SpringJavaInjectionPointsAutowiringInspection"})
@Service
public class ObjectOperationServiceImpl implements ObjectOperationService<ObjectDTO, ObjectVo> {

    private final ObjectOperationMapper objectOperationMapper;

    public ObjectOperationServiceImpl(ObjectOperationMapper objectOperationMapper) {
        this.objectOperationMapper = objectOperationMapper;
    }

    @SneakyThrows
    @Override
    public String add(ObjectVo objectVo) {

        //1.校验创建的对象,不能存在同名的对象名称
        Integer val = objectOperationMapper.selectUniqueByOrgIDObjName(objectVo.getOrgID(), objectVo.getObjName(), objectVo.getAppID());
        //如果查询到数据，那么说明创建的对象存在重名
        if (val > 0) {
            return String.format("创建的对象名称%s已经存在", objectVo.getObjName());
        }

        ObjectDTO objectDTO = new ObjectDTO();
        objectDTO.setLabel(objectVo.getLabel());
        objectDTO.setIsEnabled(objectVo.getIsEnabled());
        objectDTO.setObjName(objectVo.getObjName());
        objectDTO.setDescrption(objectVo.getDescrption());
        objectDTO.setAppID(objectVo.getAppID());
        objectDTO.setCreatedDate(new Date());
        objectDTO.setObjID(SnowFlakeUtil.getId());
        objectDTO.setCreatedBy(objectVo.getOperationID());
        objectDTO.setOwnedId(objectVo.getOperationID());
        objectDTO.setOrgID(objectVo.getOrgID());
        return objectOperationMapper.insert(objectDTO) == 1 ? objectDTO.getObjID() : "insert error";
    }

    @Override
    public ObjectDTO get(ObjectQueryVo objectQueryVo) {
        Example example = new Example(ObjectDTO.class);
        example.createCriteria().andEqualTo("objID", objectQueryVo.getObjID())
                .andEqualTo("orgID", objectQueryVo.getOrgID())
                .andEqualTo("objName", objectQueryVo.getObjName())
                .andEqualTo("appID", objectQueryVo.getAppID());
        return objectOperationMapper.selectOneByExample(example);
    }

    @Override
    public int delete(ObjectQueryVo objectQueryVo) {
        Example example = new Example(ObjectDTO.class);
        example.createCriteria().andEqualTo("objID", objectQueryVo.getObjID())
                .andEqualTo("orgID", objectQueryVo.getOrgID())
                .andEqualTo("objName", objectQueryVo.getObjName())
                .andEqualTo("appID", objectQueryVo.getAppID());
        return objectOperationMapper.deleteByExample(example);
    }

    @Override
    public int update(ObjectVo objectVo) {
        ObjectDTO objectDTO = new ObjectDTO();
        objectDTO.setLabel(objectVo.getLabel());
        objectDTO.setIsEnabled(objectVo.getIsEnabled());
        objectDTO.setObjName(objectVo.getObjName());
        objectDTO.setDescrption(objectVo.getDescrption());
        objectDTO.setAppID(objectVo.getAppID());
        objectDTO.setLastModifedDate(new Date());
        objectDTO.setLastModifedBy(objectVo.getOperationID());
        Example example = new Example(ObjectDTO.class);
        example.createCriteria().andEqualTo("objID", objectVo.getObjID())
                .andEqualTo("orgID", objectVo.getOrgID());
        return objectOperationMapper.updateByExampleSelective(objectDTO, example);
    }

    @Override
    public List<ObjectDTO> getAll(ObjectQueryVo dataQueryVo) {
        Example example = new Example(DataDTO.class);
        example.createCriteria().andEqualTo("orgID", dataQueryVo.getOrgID())
                .andEqualTo("appID", dataQueryVo.getAppID());
        return objectOperationMapper.selectByExample(example);
    }
}
