package com.mgface.metadata.design.service;

import com.mgface.metadata.design.vo.ObjectQueryVo;

import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:16
 **/
public interface ObjectOperationService<T, V> {
    String add(V objectVo);

    T get(ObjectQueryVo dataQueryVo);

    int delete(ObjectQueryVo objectQueryVo);

    int update(V objectVo);

    List<T> getAll(ObjectQueryVo dataQueryVo);
}
