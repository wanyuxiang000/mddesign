package com.mgface.metadata.design.service;

import com.mgface.metadata.design.vo.DataQueryVo;

import java.util.List;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:16
 **/
public interface DataOperationService<T, V> {
    String add(V dataVo);

    T get(DataQueryVo dataQueryVo);

    int delete(DataQueryVo dataQueryVo);

    int update(V dataVo);

    List<T> getAll(String orgID, String objectID, String appID);
}
