package com.mgface.metadata.design.datatypecheck;

import com.mgface.metadata.design.dto.FieldDTO;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 16:25
 **/
@Component
public class DataTypeValidation {
    private final List<DataTypeCheck> listvalations;
    private Map<String, DataTypeCheck> initMap = new ConcurrentHashMap<>();

    public DataTypeValidation(List<DataTypeCheck> listvalations) {
        this.listvalations = listvalations;
    }

    @PostConstruct
    public void init() {
        listvalations.forEach(val -> {
            DataType dt = val.getClass().getAnnotation(DataType.class);
            initMap.put(dt.value(), val);
        });
    }

    /**
     * 提供数据校验
     *
     * @param fieldDTOList 校验的数据格式，类型等集合
     * @param data         校验的数据来源
     */
    @SuppressWarnings("unchecked")
    public void dataTypeCheck(List<FieldDTO> fieldDTOList, Map<String, String> data) {
        for (FieldDTO val : fieldDTOList) {
            //取出字段的值
            String value = data.get(val.getFieldName());
            //取出字段对应的数据类型
            DataTypeCheck dv = initMap.get(val.getDataType());
            if (value != null && dv != null && !dv.validation(value)) {
                throw new RuntimeException(String.format("数据校验不通过->field:%s,value:%s,datatype:%s", val.getFieldName(), value, val.getDataType()));
            }
        }
    }

}
