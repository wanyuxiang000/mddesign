package com.mgface.metadata.design.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 14:01
 **/
@Data
public class ObjectQueryVo {
    @ApiModelProperty(value = "对象ID")
    private String objID;
    @ApiModelProperty(value = "组织ID", required = true)
    private String orgID;
    @ApiModelProperty(value = "APPID", required = true)
    private String appID;
    @ApiModelProperty(value = "对象名称")
    private String objName;
}
