package com.mgface.metadata.design.datatypecheck.impl;

import com.mgface.metadata.design.datatypecheck.DataType;
import com.mgface.metadata.design.datatypecheck.DataTypeCheck;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatterBuilder;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 17:22
 **/
@Component
@DataType("Date/Time")
public class DateFormatCheck implements DataTypeCheck<String> {
    private static final String pattern = "yyyy-MM-dd HH:mm:ss";

    @Override
    public boolean validation(String dateStr) {
        try {
            LocalDate.parse(dateStr, new DateTimeFormatterBuilder().appendPattern(pattern).parseStrict().toFormatter());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
