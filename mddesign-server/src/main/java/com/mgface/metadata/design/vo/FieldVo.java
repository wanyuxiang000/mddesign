package com.mgface.metadata.design.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 11:22
 **/
@Data
public class FieldVo implements Serializable {
    @ApiModelProperty(value = "字段的唯一标识")
    private String fieldID;
    @ApiModelProperty(value = "组织ID(租户ID)")
    private String orgID;
    @ApiModelProperty(value = "应用ID")
    private String appID;
    @ApiModelProperty(value = "字段所属对象名称")
    private String objName;
    @ApiModelProperty(value = "字段名")
    private String fieldName;
    @ApiModelProperty(value = "字段中文描述")
    private String descrption;
    @ApiModelProperty(value = "字段展示名称")
    private String label;
    @ApiModelProperty(value = "字段的数据类型包含:Number、TEXT、Auto Number、Date/Time、Email、Text Area 等，也包含特殊的关系类型如:Look up 关系类型，Master-Detail 关系类型等",
            required = true, allowableValues = "Number,TEXT,Auto Number,Date/Time,Email,Text Area,Look up,Master-Detail,URL,Geo")
    private String dataType;
    @ApiModelProperty(value = "用于Number、Currency、Geolocation等数字数据类型的关联设定")
    private String digitLeft;
    @ApiModelProperty(value = "小数部分的最大位数Scale")
    private String scale;
    @ApiModelProperty(value = "数据类型为TEXT时启用，指定TEXT类型的字符的长度限制")
    private String textLength;
    @ApiModelProperty(value = "DateType为关系类型(Look up,Master-Detail等)时会启用，其中RelatedTo保存关联的应用对象ID")
    private String relatedTo;
    @ApiModelProperty(value = "DateType为关系类型(Look up,Master-Detail等)时会启用,ChildRelationshipName用于保存父子关系中子方的关系名称，同一个父对象的子方的关系名称唯一，用于关系的反向查询")
    private String childRelationshipName;
    @ApiModelProperty(value = "是否必须(Y:是，N:否)", allowableValues = "Y,N")
    private String isRequired;
    @ApiModelProperty(value = "是否允许重复值(Y:是，N:否)", allowableValues = "Y,N")
    private String isUnique;
    @ApiModelProperty(value = "是否启用(Y:是,N:否)", allowableValues = "Y,N")
    private String isEnabled;
    @ApiModelProperty(value = "是否需要建索引(Y:是,N:否)", allowableValues = "Y,N")
    private String isIndexed;
    @ApiModelProperty(value = "是否作为查询条件(Y:是,N:否)", allowableValues = "Y,N")
    private String isSearched;
}
