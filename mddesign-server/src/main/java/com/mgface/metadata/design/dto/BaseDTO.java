package com.mgface.metadata.design.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-06-08 11:01
 **/
@Data
class BaseDTO {
    @ApiModelProperty("组织ID（租户ID）,如果为-1，说明是系统默认的对象。非-1的为用户自定义的对象")
    @Column(name = "orgID")
    private String orgID;

}
