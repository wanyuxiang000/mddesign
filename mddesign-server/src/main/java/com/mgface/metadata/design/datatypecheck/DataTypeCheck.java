package com.mgface.metadata.design.datatypecheck;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 16:27
 **/
@FunctionalInterface
public interface DataTypeCheck<T> {
    boolean validation(T t);
}
