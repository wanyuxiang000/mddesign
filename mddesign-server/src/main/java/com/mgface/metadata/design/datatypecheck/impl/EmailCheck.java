package com.mgface.metadata.design.datatypecheck.impl;

import com.mgface.metadata.design.datatypecheck.DataType;
import com.mgface.metadata.design.datatypecheck.DataTypeCheck;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-26 16:21
 * @desc 邮箱检验
 **/

@Component
@DataType("Email")
public final class EmailCheck implements DataTypeCheck<String> {
    private static final String regex = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    private static final Pattern p = Pattern.compile(regex);

    @Override
    public boolean validation(String mail) {
        Matcher m = p.matcher(mail);
        return m.find();
    }
}
