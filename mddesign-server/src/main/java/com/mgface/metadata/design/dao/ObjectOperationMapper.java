package com.mgface.metadata.design.dao;

import com.mgface.metadata.design.dto.ObjectDTO;
import com.mgface.metadata.design.utils.BaseTkMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.SQLException;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-07-23 19:21
 **/
@Mapper
public interface ObjectOperationMapper extends BaseTkMapper<ObjectDTO> {

    @Select("select count(objID) from objects where orgID= #{orgID} and objName = #{objName} and appID = #{appID}")
    Integer selectUniqueByOrgIDObjName(@Param("orgID") String orgID, @Param("objName") String objName, @Param("appID") String appID) throws SQLException;
}
