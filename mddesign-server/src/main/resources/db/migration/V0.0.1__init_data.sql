-- ----------------------------
-- Records of datas
-- ----------------------------
INSERT INTO `datas` (`GUID`, `orgID`, `appID`, `objID`, `name`, `value0`, `value1`, `value2`, `value3`, `value4`,
                     `value5`, `value6`, `value7`, `value8`, `value9`, `value10`, `value11`, `value12`, `value13`,
                     `value14`, `value15`, `value16`, `value17`, `value18`, `value19`, `value20`, `value21`, `value22`,
                     `value23`, `value24`, `value25`, `value26`, `value27`, `value28`, `value29`, `value30`, `value31`,
                     `value32`, `value33`, `value34`, `value35`, `value36`, `value37`, `value38`, `value39`, `value40`,
                     `value41`, `value42`, `value43`, `value44`, `value45`)
VALUES ('g000000000000001', 'A00001', '1', 'A0000000002', 'LineManage', '瑞明技术', '深圳-广州', 'T001', '常规线路', '单向发车', '一站制',
        '2', '无', '正常', '深圳', '深圳到广州的长途线路', 'yes（允许）', 'L0001', '100', '1-2-3', 'SHENZHEN', 'GUANGZHOU', '6:00',
        '21:00', '60', '5', '7:00', '9:00', '120', '5', '17:00', '19:00', '120', '5', '100', '广州-深圳', 'GUANGZHOU',
        'SHENZHEN', '6:00', '23:00', '200', '10', '6:00', '7:00', '120', '10', '17:00', '19:00', '100', '5', NULL);
INSERT INTO `datas` (`GUID`, `orgID`, `appID`, `objID`, `name`, `value0`, `value1`, `value2`, `value3`, `value4`,
                     `value5`, `value6`, `value7`, `value8`, `value9`, `value10`, `value11`, `value12`, `value13`,
                     `value14`, `value15`, `value16`, `value17`, `value18`, `value19`, `value20`, `value21`, `value22`,
                     `value23`, `value24`, `value25`, `value26`, `value27`, `value28`, `value29`, `value30`, `value31`,
                     `value32`, `value33`, `value34`, `value35`, `value36`, `value37`, `value38`, `value39`, `value40`,
                     `value41`, `value42`, `value43`, `value44`, `value45`)
VALUES ('g000000000000002', 'A00001', '1', 'A0000000001', 'Organization', 'T0000001', '租户01', '租户描述：上海人租户',
        '9999-12-31', '公司', '瑞明技术总公司', '互联网行业', '深圳', '100', '+86-15236369898', '深圳市塘朗地铁站智园A4', 'super-admin',
        '2021-07-22 14:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Records of fields
-- ----------------------------
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwm8', 'A00001', '01I2v000002zTEZ', 'CustomerName', NULL, 'CustomerName', 0, 'TEXT', NULL, NULL, 80,
        NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwmn', 'A00001', '01I2v000002zTEZ', 'CustomerNo', NULL, 'CustomerNo', 1, 'TEXT', NULL, NULL, 20,
        NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwms', 'A00001', '01I2v000002zTEZ', 'FirstName', NULL, 'FirstName', 2, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwmx', 'A00001', '01I2v000002zTEZ', 'LastName', NULL, 'LastName', 3, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwsb', 'A00001', '01I2v000002zTEj', 'OrderNo', NULL, 'OrderNo', 0, 'TEXT', NULL, NULL, 80, NULL,
        NULL, 'Y', NULL, 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwsl', 'A00001', '01I2v000002zTEj', 'Customer', NULL, 'Customer', 1, 'Lookup', NULL, NULL, NULL,
        '01I2v000002zTEZ', 'orders', 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwT0', 'A00001', '01I2v000002zTEU', 'ProductNo', NULL, 'ProductNo', 1, 'TEXT', NULL, NULL, 22, NULL,
        NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwT1', 'A00001', '01I2v000002zTEU', 'ProductName', NULL, 'ProductName', 0, 'TEXT', NULL, NULL, 22,
        NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwte', 'A00001', '01I2v000002zTEo', 'ItemPrice', NULL, 'ItemPrice', 2, 'Currency', 16, 2, NULL,
        NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwtF', 'A00001', '01I2v000002zTEo', 'Orders', NULL, 'Orders', 0, 'Master-Detail', NULL, NULL, NULL,
        '01I2v000002zTEj', 'OrderItem', 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwtj', 'A00001', '01I2v000002zTEo', 'ItemQuantity', NULL, 'ItemQuantity', 3, 'Number', 18, 0, NULL,
        NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwTL', 'A00001', '01I2v000002zTEU', 'ProductPrice', NULL, 'ProductPrice', 2, 'Currency', 16, 2,
        NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwto', 'A00001', '01I2v000002zTEo', 'OrderItemStatus', NULL, 'OrderItemStatus', 4, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwtt', 'A00001', '01I2v000002zTEj', 'OrderTime', NULL, 'OrderTime', 3, 'Date/Time', NULL, NULL,
        NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwTV', 'A00001', '01I2v000002zTEU', 'ProductStatus', NULL, 'ProductStatus', 3, 'TEXT', NULL, NULL,
        22, NULL, NULL, 'Y', 'Y', 'Y', '', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwtY', 'A00001', '01I2v000002zTEj', 'OrderStatus', NULL, 'OrderStatus', 2, 'TEXT', NULL, NULL, 80,
        NULL, NULL, 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('00N2v00000OHwtZ', 'A00001', '01I2v000002zTEo', 'Product', NULL, 'Product', 1, 'Lookup', NULL, NULL, NULL,
        '01I2v000002zTEU', 'OrderItem', 'Y', NULL, 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00001', 'A00001', 'A0000000001', 'OrgType', '组织类型', 'OrgType', 4, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00002', 'A00001', 'A0000000001', 'SuperOrgName', '上级名称', 'SuperOrgName', 5, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00003', 'A00001', 'A0000000001', 'Industry', '所属行业', 'Industry', 6, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00004', 'A00001', 'A0000000001', 'CityId', '所在城市', 'CityId', 7, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00005', 'A00001', 'A0000000001', 'LimitNum', '限制车辆数', 'LimitNum', 8, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00006', 'A00001', 'A0000000001', 'PhoneNum', '电话', 'PhoneNum', 9, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00007', 'A00001', 'A0000000001', 'Address', '地址', 'Address', 10, 'TEXT', NULL, NULL, 255, NULL, NULL, 'Y',
        'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00008', 'A00001', 'A0000000001', 'OperatorId', '录入人', 'OperatorId', 11, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('C_ORG00009', 'A00001', 'A0000000001', 'OperatorDate', '录入时间', 'OperatorDate', 12, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', NULL, NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00001', 'A00001', 'A0000000002', 'LineOrgId', '所属组织', 'LineOrgId', 0, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00002', 'A00001', 'A0000000002', 'LineName', '线路名称', 'LineName', 1, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00003', 'A00001', 'A0000000002', 'LineCode', '线路编号', 'LineCode', 2, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00004', 'A00001', 'A0000000002', 'LineType', '线路类型', 'LineType', 3, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00005', 'A00001', 'A0000000002', 'SendType', '发车类型', 'SendType', 4, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00006', 'A00001', 'A0000000002', 'LineTicketType', '线路票制', 'LineTicketType', 5, 'TEXT', NULL, NULL, 20,
        NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00007', 'A00001', 'A0000000002', 'Price', '线路票价', 'Price', 6, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y',
        'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00008', 'A00001', 'A0000000002', 'LineSubType', '线路子类型', 'LineSubType', 7, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00009', 'A00001', 'A0000000002', 'Status', '线路状态', 'Status', 8, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00010', 'A00001', 'A0000000002', 'CityId', '所在城市', 'CityId', 9, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00011', 'A00001', 'A0000000002', 'LineDesc', '线路描述', 'LineDesc', 10, 'TEXT', NULL, NULL, 20, NULL, NULL,
        'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00012', 'A00001', 'A0000000002', 'AllowCrossLine', '跨线路排班', 'AllowCrossLine', 11, 'TEXT', NULL, NULL, 20,
        NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00013', 'A00001', 'A0000000002', 'ThirdPartyLineCode', '三方平台线路编号', 'ThirdPartyLineCode', 12, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00014', 'A00001', 'A0000000002', 'UpRunInfoLineLength', '上行计划里程', 'UpRunInfoLineLength', 13, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00015', 'A00001', 'A0000000002', 'UpMapLabel', '上行线路轨迹', 'UpMapLabel', 14, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00016', 'A00001', 'A0000000002', 'UpRunInfoStartStationName', '上行首站', 'UpRunInfoStartStationName', 15,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00017', 'A00001', 'A0000000002', 'UpRunInfoEndStationName', '上行末站', 'UpRunInfoEndStationName', 16, 'TEXT',
        NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00018', 'A00001', 'A0000000002', 'UpRunInfoStartTime', '上行首班发车时间', 'UpRunInfoStartTime', 17, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00019', 'A00001', 'A0000000002', 'UpRunInfoEndTime', '上行末班发车时间', 'UpRunInfoEndTime', 18, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00020', 'A00001', 'A0000000002', 'UpRunInfoRuntime', '上行运行时长', 'UpRunInfoRuntime', 19, 'TEXT', NULL, NULL,
        20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00021', 'A00001', 'A0000000002', 'UpRunInfoDepartureInterval', '上行发车间隔', 'UpRunInfoDepartureInterval', 20,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00022', 'A00001', 'A0000000002', 'UpRunInfoMorningHighStart', '上行早峰', 'UpRunInfoMorningHighStart', 21,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00023', 'A00001', 'A0000000002', 'UpRunInfoMorningHighEnd', '上行高峰', 'UpRunInfoMorningHighEnd', 22, 'TEXT',
        NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00024', 'A00001', 'A0000000002', 'UpRunInfoMorningHighRunTime', '上行早高峰运行时长', 'UpRunInfoMorningHighRunTime',
        23, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00025', 'A00001', 'A0000000002', 'UpRunInfoMorningPeakInterval', '上行早高峰发车间隔',
        'UpRunInfoMorningPeakInterval', 24, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00026', 'A00001', 'A0000000002', 'UpRunInfoNightHighStart', '上行晚高峰(开始)', 'UpRunInfoNightHighStart', 25,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00027', 'A00001', 'A0000000002', 'UpRunInfoNightHighEnd', '上行晚高峰(结束)', 'UpRunInfoNightHighEnd', 26, 'TEXT',
        NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00028', 'A00001', 'A0000000002', 'UpRunInfoNightHighRuntime', '上行晚高峰运行时长', 'UpRunInfoNightHighRuntime', 27,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00029', 'A00001', 'A0000000002', 'UpRunInfoNightPeakInterval', '上行晚高峰发车间隔', 'UpRunInfoNightPeakInterval',
        28, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00030', 'A00001', 'A0000000002', 'DownRunInfoLineLength', '下行计划里程', 'DownRunInfoLineLength', 29, 'TEXT',
        NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00031', 'A00001', 'A0000000002', 'DownmapLabel', '下行线路轨迹', 'DownmapLabel', 30, 'TEXT', NULL, NULL, 20,
        NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00032', 'A00001', 'A0000000002', 'DownRunInfoStartStationName', '下行首站', 'DownRunInfoStartStationName', 31,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00033', 'A00001', 'A0000000002', 'DownRunInfoEndStationName', '下行末站', 'DownRunInfoEndStationName', 32,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00034', 'A00001', 'A0000000002', 'DownRunInfoStartTime', '下行首班发车时间', 'DownRunInfoStartTime', 33, 'TEXT',
        NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00035', 'A00001', 'A0000000002', 'DownRunInfoEndTime', '下行末班发车时间', 'DownRunInfoEndTime', 34, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00036', 'A00001', 'A0000000002', 'DownRunInfoRuntime', '下行运行时长', 'DownRunInfoRuntime', 35, 'TEXT', NULL,
        NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00037', 'A00001', 'A0000000002', 'DownRunInfoDepartureInterval', '下行发车间隔', 'DownRunInfoDepartureInterval',
        36, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00038', 'A00001', 'A0000000002', 'DownRunInfoMorningHighStart', '下行早峰', 'DownRunInfoMorningHighStart', 37,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00039', 'A00001', 'A0000000002', 'DownRunInfoMorningHighEnd', '下行高峰', 'DownRunInfoMorningHighEnd', 38,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00040', 'A00001', 'A0000000002', 'DownRunInfoMorningHighRunTime', '早高峰运行时长',
        'DownRunInfoMorningHighRunTime', 39, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00041', 'A00001', 'A0000000002', 'DownRunInfoMorningPeakInterval', '早高峰发车间隔',
        'DownRunInfoMorningPeakInterval', 40, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00042', 'A00001', 'A0000000002', 'DownRunInfoNightHighStart', '下行晚高峰(开始)', 'DownRunInfoNightHighStart', 41,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00043', 'A00001', 'A0000000002', 'DownRunInfoNightHighEnd', '下行晚高峰(结束)', 'DownRunInfoNightHighEnd', 42,
        'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00044', 'A00001', 'A0000000002', 'DownRunInfoNightHighRuntime', '晚高峰运行时长', 'DownRunInfoNightHighRuntime',
        43, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('LINE00045', 'A00001', 'A0000000002', 'DownRunInfoNightPeakInterval', '晚高峰发车间隔', 'DownRunInfoNightPeakInterval',
        44, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y', 'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('ORG00000', '-1', 'A0000000001', 'TenantID', '租户ID', 'TenantID', 0, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('ORG00001', '-1', 'A0000000001', 'TenantName', '租户名称', 'TenantName', 1, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('ORG00002', '-1', 'A0000000001', 'TenantDesc', '租户描述', 'TenantDesc', 2, 'TEXT', NULL, NULL, 20, NULL, NULL, 'Y',
        'Y', 'Y', 'Y', NULL);
INSERT INTO `fields` (`fieldID`, `orgID`, `objID`, `fieldName`, `descrption`, `label`, `fieldNum`, `dataType`,
                      `digitLeft`, `scale`, `textLength`, `relatedTo`, `childRelationshipName`, `isRequired`,
                      `isUnique`, `isEnabled`, `isIndexed`, `isSearched`)
VALUES ('ORG00003', '-1', 'A0000000001', 'TenantExpDate', '租户有效期', 'TenantExpDate', 3, 'TEXT', NULL, NULL, 20, NULL,
        NULL, 'Y', 'Y', 'Y', 'Y', NULL);


-- ----------------------------
-- Records of objects
-- ----------------------------
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('01I2v000002zTEj', 'A00001', '1', 'Orders', 'Orders', 'Orders', NULL, NULL, NULL, NULL, 'Y', NULL);
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('01I2v000002zTEo', 'A00001', '1', 'OrderItem', 'OrderItem', 'OrderItem', NULL, NULL, NULL, NULL, 'Y', NULL);
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('01I2v000002zTEU', 'A00001', '1', 'Product', 'Product', 'Product', NULL, NULL, NULL, NULL, 'Y', NULL);
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('01I2v000002zTEZ', 'A00001', '1', 'Customer', 'Customer', 'Customer', NULL, NULL, NULL, NULL, 'Y', NULL);
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('A0000000001', 'A00001', '1', 'Organization', 'Organization', '组织', 'admin', '2021-7-23 15:54:16', 'admin',
        '2021-7-23 15:54:19', 'Y', NULL);
INSERT INTO `objects` (`objID`, `orgID`, `appID`, `objName`, `label`, `descrption`, `createdBy`, `createdDate`,
                       `lastModifedBy`, `lastModifedDate`, `isEnabled`, `ownedId`)
VALUES ('A0000000002', 'A00001', '1', 'LineManage', 'LineManage', '线路管理', 'admin', '2021-7-23 15:51:23', 'admin',
        '2021-7-23 15:51:27', 'Y', NULL);



