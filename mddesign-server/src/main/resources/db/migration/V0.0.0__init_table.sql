/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50735
Source Host           : 127.0.0.1:13306
Source Database       : virtualddd

Target Server Type    : MYSQL
Target Server Version : 50735
File Encoding         : 65001

Date: 2021-07-23 16:48:40
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for `clobs`
-- ----------------------------
DROP TABLE IF EXISTS `clobs`;
CREATE TABLE `clobs`
(
    `clobID`   varchar(50)  NOT NULL COMMENT '超大字符唯一标识',
    `content`  text         NOT NULL,
    `checksum` varchar(255) NOT NULL COMMENT '内容校验(MD5)',
    PRIMARY KEY (`clobID`) USING BTREE,
    KEY `k_cx` (`content`(50)) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of clobs
-- ----------------------------

-- ----------------------------
-- Table structure for `datas`
-- ----------------------------
DROP TABLE IF EXISTS `datas`;
CREATE TABLE `datas`
(
    `GUID`    varchar(255) NOT NULL COMMENT '全局唯一标识',
    `orgID`   varchar(20)  NOT NULL COMMENT '组织ID(租户ID)',
    `appID`   varchar(20)  NOT NULL COMMENT '应用ID',
    `objID`   varchar(50)  NOT NULL COMMENT '对象ID',
    `name`    varchar(255) DEFAULT NULL COMMENT '数据别名',
    `value0`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum0对应的实际值',
    `value1`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum1对应的实际值',
    `value2`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum2对应的实际值',
    `value3`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum3对应的实际值',
    `value4`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum4对应的实际值',
    `value5`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum5对应的实际值',
    `value6`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum6对应的实际值',
    `value7`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum7对应的实际值',
    `value8`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum8对应的实际值',
    `value9`  varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum9对应的实际值',
    `value10` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum10对应的实际值',
    `value11` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum11对应的实际值',
    `value12` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum12对应的实际值',
    `value13` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum13对应的实际值',
    `value14` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum14对应的实际值',
    `value15` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum15对应的实际值',
    `value16` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum16对应的实际值',
    `value17` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum17对应的实际值',
    `value18` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum18对应的实际值',
    `value19` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum19对应的实际值',
    `value20` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum20对应的实际值',
    `value21` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum21对应的实际值',
    `value22` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum22对应的实际值',
    `value23` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum23对应的实际值',
    `value24` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum24对应的实际值',
    `value25` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum25对应的实际值',
    `value26` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum26对应的实际值',
    `value27` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum27对应的实际值',
    `value28` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum28对应的实际值',
    `value29` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum29对应的实际值',
    `value30` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum30对应的实际值',
    `value31` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum31对应的实际值',
    `value32` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum32对应的实际值',
    `value33` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum33对应的实际值',
    `value34` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum34对应的实际值',
    `value35` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum35对应的实际值',
    `value36` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum36对应的实际值',
    `value37` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum37对应的实际值',
    `value38` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum38对应的实际值',
    `value39` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum39对应的实际值',
    `value40` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum40对应的实际值',
    `value41` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum41对应的实际值',
    `value42` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum42对应的实际值',
    `value43` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum43对应的实际值',
    `value44` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum44对应的实际值',
    `value45` varchar(255) DEFAULT NULL COMMENT 'fileds表字段fieldsnum45对应的实际值',
    PRIMARY KEY (`GUID`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;
-- ----------------------------
-- Table structure for `fileds`
-- ----------------------------
DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields`
(
    `fieldID`               varchar(20) NOT NULL COMMENT '字段的唯一标识',
    `orgID`                 varchar(20) NOT NULL COMMENT '组织ID(租户ID)，当为-1时，说明这个字段是所有需要继承的字段',
    `objID`                 varchar(50) NOT NULL COMMENT '字段所属对象的 ObjID',
    `fieldName`             varchar(50) NOT NULL COMMENT '字段名',
    `descrption`            varchar(255) DEFAULT NULL COMMENT '字段中文描述',
    `label`                 varchar(255) DEFAULT NULL COMMENT '字段展示名称',
    `fieldNum`              int(5)       DEFAULT NULL COMMENT '对应到 Data 数据表的数据存储字段映射',
    `dataType`              varchar(20)  DEFAULT NULL COMMENT '字段的数据类型包含：Number、TEXT、Auto Number、Date/Time、Email、Text Area 等，也包含特殊的关系类型如：Look up 关系类型，Master-Detail 关系类型等',
    `digitLeft`             int(255)     DEFAULT NULL COMMENT '用于 Number、Currency、Geolocation 等数字数据类型的关联设定',
    `scale`                 int(10)      DEFAULT NULL COMMENT '小数部分的最大位数 Scale',
    `textLength`            int(255)     DEFAULT NULL COMMENT '数据类型为 TEXT 时启用，指定 TEXT 类型的字符的长度限制',
    `relatedTo`             varchar(20)  DEFAULT NULL COMMENT ' DateType 为关系类型(Look up,Master-Detail 等)时会启用，其中 RelatedTo 保存关联的应用对象 ID',
    `childRelationshipName` varchar(20)  DEFAULT NULL COMMENT 'DateType 为关系类型(Look up,Master-Detail 等)时会启用,ChildRelationshipName 用于保存父子关系中子方的关系名称，同一个父对象的子方的关系名称唯一，用于关系的反向查询',
    `isRequired`            char(1)      DEFAULT NULL COMMENT '是否必须(Y：是，N：否)',
    `isUnique`              char(1)      DEFAULT NULL COMMENT '是否允许重复值(Y：是，N：否)',
    `isEnabled`             char(1)      DEFAULT NULL COMMENT '是否启用(Y：是，N：否)',
    `isIndexed`             char(1)      DEFAULT NULL COMMENT '是否需要建索引(Y：是，N：否)',
    `isSearched`            char(1)      DEFAULT NULL COMMENT '是否作为查询条件(Y：是，N：否)',
    PRIMARY KEY (`fieldID`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for `indexes`
-- ----------------------------
DROP TABLE IF EXISTS `indexes`;
CREATE TABLE `indexes`
(
    `indexID`         varchar(255) NOT NULL,
    `orgID`           varchar(255) NOT NULL,
    `appID`           varchar(255) NOT NULL,
    `objID`           varchar(255) NOT NULL,
    `fieldNum`        int(11)      DEFAULT NULL,
    `objInstanceGUID` varchar(100) DEFAULT NULL,
    `stringValue`     varchar(255) DEFAULT NULL,
    `numValue`        int(11)      DEFAULT NULL,
    `dateValue`       datetime     DEFAULT NULL,
    PRIMARY KEY (`indexID`),
    KEY `pk_stringvalue` (`stringValue`) USING BTREE,
    KEY `pk_numvalue` (`numValue`) USING BTREE,
    KEY `pk_datevalue` (`dateValue`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of indexes
-- ----------------------------

-- ----------------------------
-- Table structure for `objects`
-- ----------------------------
DROP TABLE IF EXISTS `objects`;
CREATE TABLE `objects`
(
    `objID`           varchar(50) NOT NULL COMMENT '对象ID',
    `orgID`           varchar(20) NOT NULL COMMENT '组织ID(租户ID)',
    `appID`           varchar(10) NOT NULL COMMENT '应用ID',
    `objName`         varchar(50) NOT NULL COMMENT '对象名称',
    `label`           varchar(255) DEFAULT NULL COMMENT '对象的显示名称',
    `descrption`      varchar(255) DEFAULT NULL COMMENT '对象描述',
    `createdBy`       varchar(20)  DEFAULT NULL COMMENT '创建者',
    `createdDate`     datetime     DEFAULT NULL COMMENT '创建时间',
    `lastModifedBy`   varchar(10)  DEFAULT NULL COMMENT '最后修改者',
    `lastModifedDate` datetime     DEFAULT NULL COMMENT '最后修改时间',
    `isEnabled`       char(1)      DEFAULT NULL COMMENT '是否启用该对象(Y：启用，N：不启用)',
    `ownedId`         varchar(10)  DEFAULT NULL COMMENT '对象拥有者ID(是谁创建的，对象归属权)',
    PRIMARY KEY (`objID`) USING BTREE,
    KEY `index_combine_three_factors` (`orgID`, `appID`, `objName`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for `relationships`
-- ----------------------------
DROP TABLE IF EXISTS `relationships`;
CREATE TABLE `relationships`
(
    `orgID`               varchar(255) DEFAULT NULL,
    `objID`               varchar(255) DEFAULT NULL,
    `GUID`                varchar(255) DEFAULT NULL,
    `relationID`          varchar(255) DEFAULT NULL,
    `targetObjInstanceID` varchar(255) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of relationships
-- ----------------------------

-- ----------------------------
-- Table structure for `unique_indexes`
-- ----------------------------
DROP TABLE IF EXISTS `unique_indexes`;
CREATE TABLE `unique_indexes`
(
    `OrgID`             varchar(50)  DEFAULT NULL,
    `AppID`             int(10)      DEFAULT NULL,
    `ObjID`             varchar(50)  DEFAULT NULL,
    `FieldNum`          int(11)      DEFAULT NULL,
    `ObjInstanceGUID`   int(11)      DEFAULT NULL,
    `UniqueStringValue` varchar(255) DEFAULT NULL,
    `UniqueNumValue`    int(255)     DEFAULT NULL,
    `UniqueDateValue`   datetime     DEFAULT NULL,
    UNIQUE KEY `pk_1` (`OrgID`, `AppID`, `ObjID`, `FieldNum`, `ObjInstanceGUID`, `UniqueStringValue`) USING BTREE,
    UNIQUE KEY `pk_2` (`OrgID`, `AppID`, `ObjID`, `FieldNum`, `ObjInstanceGUID`, `UniqueNumValue`) USING BTREE,
    UNIQUE KEY `pk_3` (`OrgID`, `AppID`, `ObjID`, `FieldNum`, `ObjInstanceGUID`, `UniqueDateValue`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of unique_indexes
-- ----------------------------