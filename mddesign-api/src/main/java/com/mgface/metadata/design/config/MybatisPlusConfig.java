package com.mgface.metadata.design.config;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project bus3
 * @create 2021-07-01 17:08
 **/
@Configuration
@Slf4j
public class MybatisPlusConfig {

//    @Bean
//    public String myInterceptor(MybatisSqlSessionFactoryBean sqlSessionFactory) {
//        //实例化插件
//        MybatisSqlInterceptor sqlInterceptor = new MybatisSqlInterceptor();
//        //创建属性值
//        Properties properties = new Properties();
//        properties.setProperty("prop1", "value1");
//        //将属性值设置到插件中
//        sqlInterceptor.setProperties(properties);
//        //将插件添加到SqlSessionFactory工厂
//        sqlSessionFactory.getConfiguration().addInterceptor(sqlInterceptor);
//        return "interceptor";
//    }
}
