package com.mgface.metadata.design.controller;

import com.mgface.metadata.design.udd.UDDEngine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project mddesign
 * @create 2021-06-08 15:20
 **/
@RestController
public class TestController {
    private final UDDEngine uddEngine;

    public TestController(UDDEngine uddEngine) {
        this.uddEngine = uddEngine;
    }

    @GetMapping("/test")
    public String test() {
        return uddEngine.exec("");
    }


}
